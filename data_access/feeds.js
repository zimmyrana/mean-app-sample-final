(function() {

    function getFeeds(fee_d, options, helpers, cb) {

        helpers.getDbClient(function(error, dbClient) {
            if (!error) {
                let Feeds = dbClient.collection('feeds');
                console.log(Feeds);
                Feeds.find(fee_d).toArray(function(retrievalErr, feeds) {
                    if (!retrievalErr) {
                        helpers.execute(cb, [feeds]);
                    } else {
                        helpers.execute(retrievalErr, null);
                    }
                });

            } else {
                cb({
                    "error": "Error while connecting to mongoDB"
                })
            }
        })
    }

    function createFeeds(fee_d, options, helpers, cb, result, phon_no, email) {

        helpers.getDbClient(function(error, dbClient) {
            if (!error) {
                let Feeds = dbClient.collection('feeds');
                Feeds.find({phon_no: fee_d.phon_no, email:fee_d.email}).toArray().then((result) => {
                    //console.log('result', result.length)
                    if (result.length !== 0) {
                        helpers.execute(cb, ['already rating']);
                    } else {
                        Feeds.insertOne(fee_d, function(retrievalErr, feeds) {
                            if (!retrievalErr) {
                                helpers.execute(cb, [feeds]);
                            } else {
                                helpers.execute(retrievalErr, null);
                            }
                        });
                    }
                });
            } else {
                cb({
                    "error": "Error while connecting to mongoDB"
                })
            }
        })
    }


    exports.Feeds = {
        getFeeds: getFeeds,
        createFeeds: createFeeds
    }

})();