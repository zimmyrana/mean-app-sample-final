(function() {

    function establishRoutes(app, helpers) {

        app.use('/me', function(req, res, next) {
            require('./api/me_get').execute(req, res, next, helpers);
        });

        app.post('/brandsCreate', function(req, res, next) {
            console.log(req.brand_name);
            require('./api/brand_create').createBrand(req, res, next, helpers);
        });

        app.use('/brandsGet', function(req, res, next) {
            require('./api/brand_create').getBrand(req, res, next, helpers);
        });
        
        app.get('/login', function(req, res, next) {
            require('./api/manager_create').Managers.getManager(req, res, next, helpers);
        });


        app.post('/registration', function(req, res, next) {
            require('./api/manager_create').Managers.createManagers(req, res, next, helpers);
        });
             
        app.post('/feeds_create', function(req, res, next) {
            require('./api/feeds_create').execute(req, res, next, helpers);
        });

        app.get('/feeds', function(req, res, next) {
            require('./api/feeds_create').getdata(req, res, next, helpers);
        });      

        app.use('/get_feeds', function(req, res, next) {
        require('./api/feeds_list_get').execute(req, res, next, helpers); 
        });

        app.use('/get_managers', function(req, res, next) {
        require('./api/managers_list_get').execute(req, res, next, helpers); 
        });

       app.put('/verify/:id', function(rqst, res,next) {
        require('./api/verify_email').verifyEmail(rqst, res, next, helpers);
        });

    }

    exports.establishRoutes = establishRoutes;
})()