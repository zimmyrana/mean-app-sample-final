(function() {

    function execute(rqst, res, next, helpers) {
        const Feeds = require('../data_access/feeds').Feeds;

        Feeds.getFeedsList({}, {}, helpers, function(response) {
            res.json(response)
        })
    }

    exports.execute = execute;
})()