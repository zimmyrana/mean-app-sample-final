(function() {
    var Brands = require('../data_access/brands').Brands;

    function createBrandWithCB(rqst, res, next, helpers, callback) {
        if (rqst.body.brand_name === undefined) {
            console.log('Enter Brand Name')
        } else {
            var date = new Date();
            var brand_name = rqst.body.brand_name.trim();
            var created_date = date.toISOString();

            if (brand_name !== "" && brand_name !== null) {
                Brands.createBrands({ brand_name, created_date }, {}, helpers, function(response) {
                    callback(response)
                })
            } else {
                res.send("*Please insert Brand Name....")
                console.error();
            }
        }
    }

    function getBrandWithCB(rqst, res, next, helpers, callback) {
        Brands.getBrands({}, {}, helpers, function(response) {
            callback(response);
        })
    }

    function createBrand(rqst, res, next, helpers) {
        createBrandWithCB(rqst, res, next, helpers, response => {
            console.log(response);
            res.json(response);
        })
    }

    function getBrand(rqst, res, next, helpers) {
        getBrandWithCB(rqst, res, next, helpers, response => {
            console.log(response);
            res.json(response);
        })
    }

    exports.createBrand = createBrand;
    exports.getBrand = getBrand;
    exports.createBrandWithCB = createBrandWithCB;
    exports.getBrandWithCB = getBrandWithCB;
})()