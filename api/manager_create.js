(function() {
    function getManager(rqst, res, next, helpers) {

        const Managers = require('../data_access/managers').Managers;

        Managers.getManagersList({}, {}, helpers, function(response) {

            res.json(response)
        })
    }


    function createManagers(rqst, res, next, helpers) {
        const Managers = require('../data_access/managers').Managers;
        const bcrypt = require('bcrypt');
        var password = rqst.body.password;
        var username = rqst.body.username;
        var brand_name = rqst.body.brand_name;
        var uuid = helpers.generateUUID();
        var is_verified = false;
        var type = rqst.body.type;
        var has_access = false;

        var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var passwordFormate = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;

        if (!username.match(mailFormat)) {
            res.send("*Invailid Email Address");
        } else if (!password.match(passwordFormate)) {
            res.send("*at least one number, one lowercase, one uppercase and at least Six characters");
        } else {
            var hash = bcrypt.hashSync(password, 10);
            if (type != "1" && type != "2") {
                res.send("*Invalid type..! Please put 1 & 2");
            }
            require('./brand_create').createBrandWithCB(rqst, res, next, helpers, resp => {
                var brand_id = resp._id
                if (brand_id === true) {
                    Managers.createManagers({ "username": username, "password": hash, "uuid": uuid, "is_verified": is_verified, "brand_id": brand_id, "type": type, "has_access": has_access }, {}, helpers, function(response) {
                        if (is_verified === false) {
                            helpers.sendEmail(username, uuid);
                            res.json(response)
                        }
                    })
                } else {
                    require('./brand_create').getBrandWithCB(rqst, res, next, helpers, respo => {
                        for (var i = 0; i < respo.length; i++) {
                            var brandName = respo[i].brand_name;
                            respo[i]
                            console.log(brandName);
                            if (brand_name === brandName) {
                                brand_id = respo[i]._id;
                                Managers.createManagers({ "username": username, "password": hash, "uuid": uuid, "is_verified": is_verified, "brand_id": brand_id, "type": type, "has_access": has_access }, {}, helpers, function(response) {
                                    if (is_verified === false) {
                                        helpers.sendEmail(username, uuid);
                                        res.json(response)
                                    }
                                })
                            } else {
                                console.log(brand_name);
                                console.log(brandName);
                                console.log("Manager Not Created")
                            }
                        }
                    })
                }
            })
        }
    }


    exports.Managers = {
        getManager: getManager,
        createManagers: createManagers
    }
})()